import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './pages/contact/contact.component';
import { Page404Component } from './pages/page404/page404.component';
import { LoggedInGuard } from './shared/auth/logged-in.guard';
import { RolesGuard } from './shared/auth/roles.guard';
import { ShowDetailsResolver } from './tv/show-details/show-details.resolver';
import { ShowDetailsComponent } from './tv/show-details/show-details.component';
import { SearchComponent } from './tv/search/search.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'tv',
    children: [
      {
        path: '',
        component: SearchComponent
      },
      {
        path: ':id',
        component: ShowDetailsComponent,
        resolve: {
          show: ShowDetailsResolver
        }
      },
    ]
  },
  {
    path: 'contact',
    component: ContactComponent,
    canActivate: [
      // LoggedInGuard,
      // RolesGuard
    ],
    data: {
      roles: ['admin', 'editor']
    }
  },
  {
    path: '**',
    component: Page404Component,
    // redirectTo: '/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: false})],
  providers: [
    LoggedInGuard,
    RolesGuard
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
