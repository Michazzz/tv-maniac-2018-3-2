import { Injectable } from '@angular/core';
import { Bookmark, BookmarkId } from './bookmarks.models';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../environments/environment';

@Injectable()
export class BookmarksService<T extends Bookmark> {
  private items: T[] = [];
  private readonly apiBase = environment.bookmarksApiUrl;
  private items$ = new BehaviorSubject<T[]>([]);

  constructor(private http: HttpClient) {
    this.http.get<T[]>(this.apiBase)
      .subscribe(items => {
        this.items = items;
        this.items$.next(this.items);
      });
  }

  add(item: T): void {
    this.items.push(item);
    this.http.post(this.apiBase, item)
      .subscribe(
        () => {},
        () => {
          this.items = this.items.filter(({id}) => item.id !== id);
          this.items$.next(this.items);
        }
      );
  }

  remove(id: BookmarkId): void {
    this.http.delete(`${this.apiBase}/${id}`)
      .subscribe(() => {
        this.items = this.items.filter(item => item.id !== id);
        this.items$.next(this.items);
      });
  }

  getAll(): Observable<T[]> {
    return this.items$.asObservable();
  }

  has(id: BookmarkId): boolean {
    return this.items.some(item => item.id === id);
  }
}
