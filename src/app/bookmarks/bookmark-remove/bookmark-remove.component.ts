import { Component, Input } from '@angular/core';
import { BookmarksService } from '../bookmarks.service';
import { Bookmark } from '../bookmarks.models';

@Component({
  selector: 'ma-bookmark-remove',
  templateUrl: './bookmark-remove.component.html',
  styleUrls: ['./bookmark-remove.component.scss']
})
export class BookmarkRemoveComponent {
  @Input() item: Bookmark;

  constructor(private bs: BookmarksService<Bookmark>) {
  }

  remove() {
    this.bs.remove(this.item.id);
  }

  has(): boolean {
    return this.bs.has(this.item.id);
  }
}
