import { Directive, ElementRef, HostBinding, HostListener, Input } from '@angular/core';
import { BookmarksService } from './bookmarks.service';
import { Bookmark } from './bookmarks.models';

@Directive({
  selector: '[maBookmarked]',
  exportAs: 'bookmarked'
})
export class BookmarkedDirective {
  @Input('maBookmarked') item: Bookmark; // tslint:disable-line:no-input-rename

  constructor(private bs: BookmarksService<Bookmark>,
              private el: ElementRef) {
    (el.nativeElement as HTMLElement)
      .classList.add('hello-native');
  }

  @HostBinding('class.bookmarked')
  get isBookmarked() {
    return this.bs.has(this.item.id);
  }

  @HostBinding('class.opaque')
  isOpaque = false;

  @HostListener('click', ['$event.target'])
  toggleOpaque(target: HTMLElement) {
    if (target.tagName === 'IMG') {
      this.isOpaque = !this.isOpaque;
    }
  }
}
