import { Component, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'ma-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent {
  pending = false;

  constructor(router: Router) {
    router.events
      .subscribe(event => {
        if (event instanceof NavigationStart) {
          this.pending = true;
        }
        if (event instanceof NavigationEnd) {
          this.pending = false;
        }
      });
  }
}
