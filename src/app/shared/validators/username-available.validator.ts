import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { ajax } from 'rxjs/observable/dom/ajax';
import { delay, map } from 'rxjs/operators';

export function usernameAvailableValidator(caseSensitive = false): AsyncValidatorFn {
  return (control: AbstractControl): Observable<ValidationErrors | null> => {
    const apiEndpoint = 'https://jsonplaceholder.typicode.com/users';

    const validity = users => caseSensitive
      ? users.some(user => user.username === control.value)
      : users.some(user => user.username.toLocaleLowerCase() === control.value.toLowerCase());

    const validationResult = invalid => invalid
      ? {
        usernameAvailable: true
      }
      : null;

    return ajax(apiEndpoint)
      .pipe(
        delay(1000),
        map(({response}) => response),
        map(validity),
        map(validationResult)
      );
  };
}
