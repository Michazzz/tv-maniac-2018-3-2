import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { startsWithLetterValidator } from './starts-with-letter.validator';

@Directive({
  selector: '[maStartsWithLetter]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: StartsWithLetterDirective, multi: true}  // <- NG_ASYNC_VALIDATORS
  ]
})
export class StartsWithLetterDirective implements Validator {                      // <- AsyncValidator
  @Input('maStartsWithLetter') upperOnly = false; // tslint:disable-line:no-input-rename

  validate(control: AbstractControl): ValidationErrors | null {
    return startsWithLetterValidator(this.upperOnly)(control);
  }
}
