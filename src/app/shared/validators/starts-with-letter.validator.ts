import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function startsWithLetterValidator(upperOnly = false): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const rule = upperOnly
      ? /^[A-Z]/
      : /^[a-zA-Z]/;
    return !control.value || rule.test(control.value)
      ? null
      : {
        startsWithLetter: control.value[0]
      };
  };
}
