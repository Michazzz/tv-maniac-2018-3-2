import { Directive } from '@angular/core';
import { AbstractControl, AsyncValidator, NG_ASYNC_VALIDATORS, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { usernameAvailableValidator } from './username-available.validator';

@Directive({
  selector: '[maUsernameAvailable]',
  providers: [
    {provide: NG_ASYNC_VALIDATORS, useExisting: UsernameAvailableDirective, multi: true}
  ]
})
export class UsernameAvailableDirective implements AsyncValidator {
  validate(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return usernameAvailableValidator()(control);
  }
}
