import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RolesGuard implements CanActivate {
  canActivate(route: ActivatedRouteSnapshot): boolean {
    const currentRoles = ['user', 'admin', 'vip', 'editor'];
    const requiredRoles = route.data.roles;

    return requiredRoles.every(
      requiredRole => currentRoles.some(current => current === requiredRole)
      // requiredRole => currentRoles.includes(requiredRole)
    );
  }
}
