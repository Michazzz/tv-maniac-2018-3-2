import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, tap } from 'rxjs/operators';
import { NotificationsService } from '../notifications/notifications.service';
import { Injectable } from '@angular/core';
import { _throw } from 'rxjs/observable/throw';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private ns: NotificationsService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(
        tap(event => console.log('[Http]', event)),
        catchError(err => {
          const message = {
            0: 'Your internet is broken',
            404: 'The resource is not there',
            fallback: 'Unknown evil occurred'
          }[err.status || 'fallback'];

          this.ns.error('API request error: ' + message);
          return _throw(err);
        })
      );
  }
}
