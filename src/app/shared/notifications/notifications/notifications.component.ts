import { Component, OnInit } from '@angular/core';
import { NotificationsService } from '../notifications.service';
import { Observable } from 'rxjs/Observable';
import { Notification } from '../notifications.models';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'ma-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  notification$: Observable<Notification>;

  constructor(private ns: NotificationsService) {
    this.notification$ = this.ns.notification;
  }

  ngOnInit() {
  }

  hide() {
    this.ns.notification.next(null);
  }
}
