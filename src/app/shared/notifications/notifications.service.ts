import { Injectable } from '@angular/core';
import { Notification, NotificationType } from './notifications.models';
import { Subject } from 'rxjs/Subject';
import { tap } from 'rxjs/operators';

@Injectable()
export class NotificationsService {
  notification = new Subject<Notification>();

  message(text: string) {
    this.triggerNotification(text, NotificationType.Message);
  }

  info(text: string) {
    this.triggerNotification(text, NotificationType.Info);
  }

  error(text: string) {
    this.triggerNotification(text, NotificationType.Error);
  }

  warning(text: string) {
    this.triggerNotification(text, NotificationType.Warning);
  }

  private triggerNotification(message: string, type: NotificationType) {
    this.notification.next({message, type});
    setTimeout(() => this.notification.next(null), 3000);
  }
}
