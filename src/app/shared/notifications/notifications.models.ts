export enum NotificationType {
  Message = 'primary',
  Info = 'info',
  Warning = 'warning',
  Error = 'danger'
}

export interface Notification {
  type: NotificationType;
  message: string;
}
