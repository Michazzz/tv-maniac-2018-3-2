import { animate, query, stagger, style, transition, trigger } from '@angular/animations';

export class Animations {
  static zoomInOut = trigger('zoomInOut', [
    transition(':enter', [
      style({transform: 'scale(0)'}),
      animate('100ms', style({transform: 'scale(1)'}))
    ]),
    transition(':leave', [
      style({
        transform: 'translateX(0) scale(1) rotate(0)'
      }),
      animate('300ms', style({transform: 'translateX(-200px) scale(0) rotate(-360deg)'}))
    ])
  ]);

  static staggerDown = trigger('staggerDown', [
    transition('* => *', [
      query(':enter', [
        style({transform: 'translateY(-75px) scale(0)'}),
        stagger(60, [
          animate('120ms', style({transform: 'translateY(0) scale(1)'}))
        ])
      ], {optional: true }),
      query(':leave', [
        style({transform: 'translateY(0) scale(1)'}),
        stagger(60, [
          animate('120ms', style({transform: 'translateY(200px) scale(0)'}))
        ])
      ], {optional: true })
    ])
  ]);
}
