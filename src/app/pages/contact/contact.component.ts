import { Component, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'ma-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contact = {
    email: '',
    message: 'a'
  };

  constructor() {
  }

  ngOnInit() {
  }

  send() {
    console.log('Sending form...');
  }

  showErrors(control: AbstractControl): boolean {
    return (control.dirty || control.touched) && control.invalid;
  }

}
