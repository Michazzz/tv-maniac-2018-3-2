import { Pipe, PipeTransform } from '@angular/core';
import { Episode } from '../tv.models';
import { padStart } from 'lodash';

@Pipe({
  name: 'episodise'
})
export class EpisodisePipe implements PipeTransform {

  transform({number, season}: Episode, lowercased = false): string {
    // const episodePadded = padStart(number.toString(), 2, '0');
    // const seasonPadded = padStart(season.toString(), 2, '0');

    const [episodePadded, seasonPadded] = [number, season]
      .map(prop => padStart(prop.toString(), 2, '0'));

    return lowercased
      ? `s${seasonPadded}e${episodePadded}`
      : `S${seasonPadded}E${episodePadded}`;
  }

}
