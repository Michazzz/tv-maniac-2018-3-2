import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, ViewEncapsulation } from '@angular/core';
import { get } from 'lodash';
import { Show } from '../tv.models';
import { ActivatedRoute } from '@angular/router';
import { ShowDetailsComponent } from '../show-details/show-details.component';

type PosterSize = 'lg' | 'md';
type ShowImageSize = 'original' | 'medium';

interface PosterSizeDict {
  [size: string]: ShowImageSize;
}

@Component({
  selector: 'ma-poster',
  templateUrl: './poster.component.html',
  styleUrls: ['./poster.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PosterComponent implements OnChanges {
  @Input() show: Show;
  @Input() size: PosterSize = 'lg';
  posterUrl: string;
  private readonly placeholder = 'http://fillmurray.com/400/600';

  constructor(private route: ActivatedRoute) {
  }

  ngOnChanges() {
    const sizesDict: PosterSizeDict = {
      lg: 'original',
      md: 'medium'
    };

    const sizeKey = sizesDict[this.size];

    this.posterUrl = get(this.show, ['image', sizeKey], this.placeholder);
  }

  displayName(): boolean {
    return this.route.snapshot.component !== ShowDetailsComponent;
  }
}
