import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { Show, ShowDetails, ShowResponse } from './tv.models';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable()
export class TvMazeService {
  private readonly apiRoot = 'https://api.tvmaze.com';

  constructor(private http: HttpClient) { }

  searchShows(query: string): Observable<Show[]> {
    return this.http.get<ShowResponse[]>(`${this.apiRoot}/search/shows?q=${query}`)
      .pipe(
        map(showsResponses => showsResponses.map(({show}) => show))
      );
  }

  getShow(id: string): Observable<ShowDetails> {
    return this.http.get<ShowDetails>(`${this.apiRoot}/shows/${id}?embed=episodes`);
  }
}
