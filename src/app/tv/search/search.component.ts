import { Component } from '@angular/core';
import { Show } from '../tv.models';
import { TvMazeService } from '../tv-maze.service';
import { BookmarksService } from '../../bookmarks/bookmarks.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime, filter, map, merge, startWith, tap } from 'rxjs/operators';
import { startsWithLetterValidator } from '../../shared/validators/starts-with-letter.validator';
import { usernameAvailableValidator } from '../../shared/validators/username-available.validator';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Animations } from '../../shared/animations';

@Component({
  selector: 'ma-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  animations: [
    trigger('bookmarked', [
      state('no', style({opacity: 0.8})),
      state('yes', style({opacity: 1})),
      transition('no => yes', animate('200ms ease-in')),
      transition('yes => no', animate('500ms ease-out')),
    ]),
    Animations.zoomInOut,
    Animations.staggerDown
  ]
})
export class SearchComponent {
  shows: Show[];
  query = 'flash';
  form: FormGroup;
  click$ = new Subject<void>();
  bookmarks$: Observable<Show[]>;

  constructor(private tv: TvMazeService,
              private bs: BookmarksService<Show>,
              private fb: FormBuilder) {
    this.bookmarks$ = this.bs.getAll();

    // const queryControl = this.fb.control('batman');
    this.form = this.fb.group({
      query: ['batman', [
        Validators.required,
        startsWithLetterValidator(false)
      ], [
        usernameAvailableValidator()
      ]]
    });

    this.form.valueChanges
      .pipe(
        startWith(1),
        merge(this.form.statusChanges),
        merge(this.click$),
        map(() => this.form.value.query),
        debounceTime(200),
        tap(() => this.form.controls.query.errors && console.log(this.form.controls.query.errors)),
        filter(() => this.form.valid)
      )
      .subscribe(this.search);
  }

  private search = (query: string) => {
    this.tv.searchShows(query)
      .subscribe(shows => this.shows = shows);
  }
}
