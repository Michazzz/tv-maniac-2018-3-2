import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { ShowDetails } from '../tv.models';
import { Observable } from 'rxjs/Observable';
import { TvMazeService } from '../tv-maze.service';
import { Injectable } from '@angular/core';
import { ShowDetailsParams } from '../tv.module';

@Injectable()
export class ShowDetailsResolver implements Resolve<ShowDetails> {
  constructor(private tv: TvMazeService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<ShowDetails> {
    const {id} = route.params as ShowDetailsParams;
    return this.tv.getShow(id);
  }
}
