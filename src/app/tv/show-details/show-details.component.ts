import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TvMazeService } from '../tv-maze.service';
import { ShowDetails } from '../tv.models';

@Component({
  selector: 'ma-show-details',
  templateUrl: './show-details.component.html',
  styleUrls: ['./show-details.component.scss']
})
export class ShowDetailsComponent implements OnInit {
  show: ShowDetails;

  constructor(private route: ActivatedRoute,
              private tv: TvMazeService) {
    this.show = this.route.snapshot.data.show;
  }

  ngOnInit() {
  }

}
