import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search/search.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TvMazeService } from './tv-maze.service';
import { PosterComponent } from './poster/poster.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BookmarksModule } from '../bookmarks/bookmarks.module';
import { ShowDetailsComponent } from './show-details/show-details.component';
import { RouterModule, Routes } from '@angular/router';
import { EpisodisePipe } from './pipes/episodise.pipe';
import { ShowDetailsResolver } from './show-details/show-details.resolver';
import { ErrorInterceptor } from '../shared/interceptors/error.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared/shared.module';

export interface ShowDetailsParams {
  id: string;
}

const routes: Routes = [];

@NgModule({
  imports: [
    HttpClientModule,
    SharedModule,
    RouterModule.forChild(routes),
    BookmarksModule,
  ],
  declarations: [SearchComponent, PosterComponent, ShowDetailsComponent, EpisodisePipe],
  providers: [
    TvMazeService,
    ShowDetailsResolver,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    }
  ]
})
export class TvModule {
}
