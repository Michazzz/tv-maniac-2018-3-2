import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NotificationsService } from '../shared/notifications/notifications.service';

@Component({
  selector: 'ma-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  constructor(private ns: NotificationsService) {
  }

  triggerNotification() {
    this.ns.error('This is the triggered error');
  }
}
