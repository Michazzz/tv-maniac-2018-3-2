import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './pages/contact/contact.component';
import { Page404Component } from './pages/page404/page404.component';
import { FormsModule } from '@angular/forms';
import { LoadingComponent } from './shared/components/loading/loading.component';
import { SubmitIfValidDirective } from './shared/forms/submit-if-valid.directive';
import { StartsWithLetterDirective } from './shared/validators/starts-with-letter.directive';
import { UsernameAvailableDirective } from './shared/validators/username-available.directive';
import { NotificationsModule } from './shared/notifications/notifications.module';
import { configInitializer } from './config.initializer';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ConfigService } from './shared/config.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared/shared.module';
import { TvModule } from './tv/tv.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    Page404Component,
    LoadingComponent,
    SubmitIfValidDirective,
    StartsWithLetterDirective,
    UsernameAvailableDirective,
  ],
  imports: [
    TvModule,
    AppRoutingModule,
    FormsModule,
    NotificationsModule,
    HttpClientModule,
    SharedModule,
    BrowserModule,
    BrowserAnimationsModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: configInitializer,
      deps: [HttpClient, ConfigService],
      multi: true,
    },
    ConfigService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
