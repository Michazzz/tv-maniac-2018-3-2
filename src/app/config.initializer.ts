import { HttpClient } from '@angular/common/http';
import { ConfigService } from './shared/config.service';

export function configInitializer(http: HttpClient, config: ConfigService) {
  return (): Promise<any> =>
    http.get<any>('./config.json')
      .toPromise()
      .then(data => config.version = data.version);
}
