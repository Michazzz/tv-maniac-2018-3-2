import { Component } from '@angular/core';

@Component({
  selector: 'ma-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Hello from app';

  handleClick(value: string) {
    this.title = value;
  }

  log(msg) {
    console.log(msg);
  }
}
