# Resources:

## Animations:
- https://angular.io/guide/animations
- https://www.yearofmoo.com/2017/06/new-wave-of-animation-features.html

## Playground:
- http://www.angularplayground.it
- https://storybook.js.org

# NativeScript:
- https://docs.nativescript.org
- https://github.com/NathanWalker/angular-seed-advanced

# Docs:
- https://angular.io/guide/cheatsheet
- https://angular.io/guide/styleguide

# StackBlitz & friends:
- https://stackblitz.com
- https://ng-run.com/
- http://codesandbox.io/

# Podcasts:
- https://devchat.tv/

# RxJS: 
- http://rxmarbles.com/#reduce